let Number = ["55", "65", "75"];

// add "20" at the beginning of the array
Number.unshift("20");
console.log(Number);

// Output: [ '20','55', '65', '75', '' ]


// string of array

let items = ["JavaScript", 1, "a", 3];

let itemsString = items.toString();

console.log(itemsString);

// Output: 
// JavaScript,1,a,3



// count the length of words //




function numberofWords(sentence){
    const words = sentence.split(" ")

    return `The number of words is ${words.length}`

}

let sentence = "Hello learning Javascript"
console.log(numberofWords(sentence))



// sum of negtive numbers //


var numbers = [-1, -2, -3, 4, 5, 6];

var result = 0;

for (var i = 0; i < numbers.length; ++i) {
    if (numbers[i] < 0) {
        result += numbers[i];
    }
}

console.log(result); // -6



// sum of negtive numbes //


const sumNegative = (numbers) => {
    let result = 0;
    for (let i = 0; i < numbers.length; ++i) {
        if (numbers[i] < 0) {
            result += numbers[i];
        }
    }
    return result;
};


const result = sumNegative([-1, -2, -3, 4, 5, 6]);

console.log(result); // -6 //