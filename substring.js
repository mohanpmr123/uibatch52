console.log("substring")

// word length using substring //

function filterStringsByLength(arrayOfStrings, length) {
    return arrayOfStrings.filter((str) => str.length > length);
  }
  
  const inputArray = ["java", "Python", "HTMl", "CSS"];
  const lengthThreshold =5;
  const outputArray = filterStringsByLength(inputArray, lengthThreshold);
  
  console.log("Output:", outputArray);

  // substring for sentences //
  function isSubstringPresent(sentence, substring) {
    return sentence.includes(substring);
}

const sentence = "Hello world";
const substring = "world";

if (isSubstringPresent(sentence, substring)) {
    console.log("The substring is present in the sentence.");
} else {
    console.log("The substring is NOT present in the sentence.");
}


//Data types
//let,var,const
//Conditional statements
//if,ifelse,switch
//for loop ,for each
//while
//Problem statements by using above concepts
//Git 
//Arrow functions,higher order functions
//DOM
//Git clone


//Functions
// function square(){
    
// }

//Arrow functions


const square = (num)=>{
    return num
}
square(2);


//higher Order functions
//map ,reduce,filter
const arr = [1,2,3,4,5,6,7,8,9,10]
//Map
let squared_arr =arr.map((num)=>num*num);
console.log(squared_arr);
//Filter
let even_number= arr.filter((num)=>num%2 ==0);
console.log(even_number);

//Reduce
let sumofelements = arr.reduce((sum,num)=>sum+num,0);
console.log(sumofelements)

//find cube of each element in an array
//return only odd elements in an array
//product of each element in an array
//find the element which are more than given specified value
//["helo","hii","bye"] === hello
let numbers = [1,3,4,5,6,7,20]
let words = ["helo","hii","bye"]
let cubes = numbers.map((num)=>num*num*num);
let oddnumbers = numbers.filter((num)=>num%2!=0);
let product  = numbers.reduce((pro,num)=>pro*num ,1);
let specified_length_ele = words.filter((word)=>word.length >3);
console.log(cubes);
console.log(oddnumbers);
console.log(product);
console.log(specified_length_ele);



// operators //
a=10
b=20;
c=a+b
console.log("addition:"+c)
c=a-b
console.log("substrction:"+c)
c=a*b
console.log("multiplication:"+c)
c=a%b
console.log("modules:"+c)









